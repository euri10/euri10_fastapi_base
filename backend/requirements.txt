-i https://pypi.org/simple
alembic==1.0.11
amqp==2.5.0
argon2-cffi==19.1.0
asn1crypto==0.24.0
asyncpg==0.18.3
billiard==3.6.0.0
celery==4.3.0
cffi==1.12.3
cryptography==2.7
databases[postgresql]==0.2.5
dnspython==1.16.0
email-validator==1.0.4
faker==2.0.0
fastapi==0.34.0
idna==2.8
kombu==4.6.3
mako==1.1.0
markupsafe==1.1.1
passlib[argon2]==1.7.1
psycopg2-binary==2.8.3
pycparser==2.19
pydantic[email]==0.30
pyjwt[crypto]==1.7.1
python-dateutil==2.8.0
python-editor==1.0.4
python-multipart==0.0.5
pytz==2019.2
pyyaml==5.1.2
six==1.12.0
sqlalchemy==1.3.6
starlette==0.12.7
text-unidecode==1.2
vine==1.3.0
